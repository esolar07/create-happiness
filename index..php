	<?php include ('chheader.php'); ?>
	
	<?php include ('chmenu.php'); ?>

	<!-- Nav End -->
	
	<div id="main" class='container main'>
		<img src="images/main_image.jpg" style='max-width: 100%  height: auto;' class="img-responsive" alt="Responsive image">
	</div>

	<div class="divider1" class="row">
		<p class="divtext"> Happiness * Inspiration * Peace & Love</p>
	</div>

	<div id="about" class="container about">
		<p>I designed and originated my “Create Happiness” book throughout my junior year of high school as a mode of relaxation. Each doodle illustration took hours to create and was a thoughtful escape from today’s blur of social media and other pressures of daily life. As an avid doodler and creative type, I wanted to offer a unique way to encourage and inspire other teens like myself. I have begun delivering my “Create Happiness” book to young people in Miami who I believe can enjoy this same type of break from the constant pressures of teen life. My doodles in this book offer uplifting, entertaining and engaging messages for the artist in each of us!</p><br>
		<p>Please enjoy my doodles… color them in and make them your own artistic masterpiece….cut them out and display them in a place of honor where you can focus on the inspiring message of your own creation!</p></br>
		<p>“Create Happiness” wherever you are. I know sometimes life is really hard but I do believe that we can create our own happiness through positive thoughts and our own positive actions.</p>
	</div>

	<div class="divider1" class="row">
		<p class="divtext"> Achievement * Confidence * Relaxation</p>
	</div>

	<div id="photos" class="container">
		<div class="row photos">
			<div class="col-lg-3">
				<img class="pics" src="images/pic5.jpg" style='max-width: 100%;  height: auto;'>
			</div>
			<div class="col-lg-3">
				<img class="pics" src="images/pic3.jpg" style='max-width: 100%;  height: auto;'>
			</div>
			<div class="col-lg-3">
				<img class="pics" src="images/pic2.jpg" style='max-width: 100%;  height: auto;'>
			</div>
			<div class="col-lg-3">
				<img class="pics" src="images/pic4.jpg" style='max-width: 100%;  height: auto;'>
			</div>
		</div>
	</div>
	
	<div class="divider1" class="row">
		<p class="divtext"> Focus on the inspiring message of your own creation!</p>
	</div>
	
	<div class="container">
		<div class="row">
			<p> Contact Us</p>
		</div>
		<div id="contact" class="row">
			<div class="col-lg-5">
				<img src="images/ch.jpg" style='max-width: 100%;  height: auto; border: 5px solid black; border-radius: 150px;' class="img-responsive" alt="Responsive image"><br>
				<h1>Jenna Fusfield</h1><br>
				<p>"Create Happiness" offers an outlet for teens to bring positivity into their lives through their own creativity.</p>

			</div>
			<div class='col-lg-7'>
			<form role="form">
				<div class="form-group">
					<input type="text" name="name" placeholder="Name">
				</div>
				<div class="form-group">
					<input type="text" name="email" placeholder="Email">
				</div>
					<textarea class="form-control" row="10" col="30" name="message"></textarea><br>
				<button type="submit" class="btn btn-default" style="background-color:black; color:white;">Send Message</button>
			</form><br>
			</div>

			<p> Visit us at <a href="https://www.facebook.com/pages/Create-Happiness/321932481273002" target="_blank"><img src="images/socialfb.png" alt="FaceBook"></a></p>
			<div class="fb-like" data-href="https://www.facebook.com/pages/Create-Happiness/321932481273002" data-layout="standard" data-action="like" data-show-faces="true" data-share="true"></div>

		</div>
	</div>

	<div>
		<img src="images/picfoot.jpg" style='max-width: 100%;  height: auto;' class='img-reponsive' alt="Bottom Pic">
	</div>

</body>
</html>