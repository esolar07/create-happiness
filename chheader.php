<!DOCTYPE HTML>
<html lang="eng">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href='http://fonts.googleapis.com/css?family=The+Girl+Next+Door' rel='stylesheet' type='text/css'>
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
    </script>
    <script href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css">
    </script>
	<link href="bootstrap.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="ch.css">

	<script type="text/javascript">
		function validate() {

			//collecting data from form
			name = document.getElementById('chname').value;
			email = document.getElementById('chemail').value;
			message = document.getElementById('chmessage').value;

			errors = "";

			// checking data from form (name -> email -> message)
			numRE = /\d/;
			if (name=="" || name == null || name.match(numRE)){
				errors += "Must enter your name. \n";
			}// end of name if 

			emailRE = /^.+@.+\..{2,4}$/;
			if (email.match(emailRE)){
				console.log ('email success');
			} else {
				errors += "Must enter a valid email. \n";
			} // end of email if

			if (message == "" || message == null){
				errors += "Must enter a message \n";
			} else {
				console.log ("message success");
			} // end of message if

			// checking for any errors
			if (errors == ""){
				console.log ("success!");
				}else {
					alert (errors);
			} // end of errors if
		} // end of main function
	</script>
</head>

<body>
	<div id="fb-root"></div>
		<script>
		(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	    </script>

	<!-- Nav -->
	<div>
		<img src="images/pichead.jpg" style='max-width: 100%;  height: auto;' class='img-reponsive' alt="Top Pic">
	</div>
